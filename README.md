# README #
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Requirements ###

Construct news portal that should be counterpart of http://www.bbc.com/news/ portal following the same design and with the following features:

- Content editors(people who are responsible for content setup)​ can login into system and add/edit/remove news.
- System should support user flows so news will appear in public only if approved by people with "content approver" role. (Content editors can have that role but not it is not necessary).
- Every news should have category and language assigned so i can search for American technology news or Japanese political news. Initially site should support 3 languages: English, Russian and Chinese.
- Landing page should contain some hero articles that are configurable by content editors. Set of articles visible on landing page should be also configurable.
- Solution should be scalable to be able to sustain peaks, easily ramp-up/ramp-down new application instances. Also pages should load at reasonable amount of time(5 secs is optimal).

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### How do I get set up? ###

-- Install apps:

* Maven
* nginx
 
-- Execute commands:

* Compile and install all maven artefacts
```
#!bash

mvn clean install
```
* Run Author app http://localhost:8088
```
#!bash

run_author_app.sh
```
* Run Public 0 app http://localhost:8089
```
#!bash

run_public_app.sh
```
* Run Public 1 app http://localhost:8090
```
#!bash

run_public1_app.sh
```

* Run Load Balancer (nginx should be installed) http://localhost:1025
```
#!bash

run_load_balancer.sh
```


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Accounts: ###

 - Superuser: login: `superuser`; password: `superuser`

 - Content Editor: login: `eric`; password: `eric`

 - Content Approver: login: `peter`; password: `peter`



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Design ###



> Construct news portal that should be counterpart of http://www.bbc.com/news/ portal following the same design:

I wasn't keen to apply the original design - but made the one that looks close to it



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Content Editor role ###

> - Content editors(people who are responsible for content setup)​ can login into system and add/edit/remove news.

User `eric` as Content Editor



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Content Approver role ###

> - System should support user flows so news will appear in public only if approved by people with "content approver" role. (Content editors can have that role but not it is not necessary).

User `peter` as Content Approver



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Search by categories ###
> - Every news should have category and language assigned so i can search for American technology news or Japanese political news. 

Categories:

  - Common:  sport, technologies, politics, business

  - Countries: usa, russia, germany, japan

Multiple categories can be assigned to any "BBCNews" page;

Users are able to search by categories in two ways:

1. Links in the navigation menu.

2. Via Search box: 

 -  By news content

 -  By localized category`s display names 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
###  I18N ### 

> Initially site should support 3 languages: English, Russian and Chinese.

 - English (default) http://localhost:8088/news/ - translation exists

 - Russian http://localhost:8088/ru/news/ - translation exists

 - German http://localhost:8088/de/news/ - content translation doesn`t exists

 - Chinese http://localhost:8088/zh_CN/news/ - content translation doesn`t exists




---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Landing page customisation ###

> - Landing page should contain some hero articles that are configurable by content editors. Set of articles visible on landing page should be also configurable.

Components `Teaser Hero` and `Internal Page Teaser` can be used for these purposes.



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Scalability ###

> - Solution should be scalable to be able to sustain peaks, easily ramp-up/ramp-down new application instances. Also pages should load at reasonable amount of time(5 secs is optimal).

Two Public instances can be launched by `run_public_app.sh`, `run_public1_app.sh` scripts;

Load balancer can be launched by `run_load_balancer.sh` script;

To add/deploy new Public instance (except existing two) you can do the next:

 - Copy the public instance module

 - Change tomcat port (in the file `pom.xml` by path `build.plugins.plugin.configuration.port` where `build.plugins.plugin.configuration.artifactId` is `tomcat7-maven-plugin` ) 

 - Launch the instance

 - Register the new instance as subscriber in the Author app

 - Republish all changes 

 - Update nginx config `load-balancer/nginx/conf/nginx.conf` - add new public instance 

 - Restart nginx




** Assumptions: **

 - Public apps doesnt allow to save data. Thus there is no need to create Jackrabbit's cluster of public instances. Author app will update all public instances by subscriptions.


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
