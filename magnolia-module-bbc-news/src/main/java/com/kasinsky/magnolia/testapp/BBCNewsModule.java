package com.kasinsky.magnolia.testapp;

/**
 * This class is optional and represents the configuration for the magnolia-module-bbc-news module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/magnolia-module-bbc-news</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class BBCNewsModule {
    /* you can optionally implement info.magnolia.module.ModuleLifecycle */

}
