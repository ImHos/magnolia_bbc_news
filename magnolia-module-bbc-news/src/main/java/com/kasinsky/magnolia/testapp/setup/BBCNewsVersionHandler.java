package com.kasinsky.magnolia.testapp.setup;

import com.kasinsky.magnolia.testapp.module.categorization.model.SearchWithCategoryResultModel;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.IsAuthorInstanceDelegateTask;
import info.magnolia.module.delta.NodeExistsDelegateTask;
import info.magnolia.module.delta.RemoveNodeTask;
import info.magnolia.module.delta.RenameNodesTask;
import info.magnolia.module.delta.SetPropertyTask;
import info.magnolia.module.delta.Task;
import info.magnolia.repository.RepositoryConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manager the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class BBCNewsVersionHandler extends DefaultModuleVersionHandler {
    private static final String PATH_TO_I18N = "/server/i18n/content";
    private static final String PATH_TO_CATEGORIZATION_I18N = "modules/categorization/apps/categories/subApps/detail/editor/form/tabs/category/fields/displayName";
    private static final String PATH_TO_SEARCH_RESULT_COMPONENT = "modules/standard-templating-kit/templates/components/features/stkSearchResult";
    private static final String PATH_DAM_APP_PERMISSIONS = "modules/dam-app/apps/assets/permissions/roles";
    private static final String PATH_TO_THEME = "modules/standard-templating-kit/config/site/theme";
    private static final String PATH_TO_DEFAULT_SUBSCRIBER = "/server/activation/subscribers/magnoliaPublic8089";
    private static final String PATH_TO_MAIN_AREAS_INTRO = "modules/standard-templating-kit/config/site/templates/prototype/areas/main/areas/intro";
    public static final String PATH_TO_CATEGORY_OVERVIEW_TEMPLATE = "modules.standard-templating-kit.templates.components.features.stkCategoryOverview";

    @Override
    protected List<Task> getExtraInstallTasks(InstallContext ctx) {
        final List<Task> tasks = new ArrayList<Task>();
        tasks.addAll(super.getExtraInstallTasks(ctx));
        tasks.add(new SetPropertyTask("Update content i18n: set enabled=true", RepositoryConstants.CONFIG, PATH_TO_I18N, "enabled", "true"));
        tasks.add(new SetPropertyTask("Update Categorization app i18n: set enabled=true", RepositoryConstants.CONFIG, PATH_TO_CATEGORIZATION_I18N, "i18n", "true"));
        SetPropertyTask setModelClassTask = new SetPropertyTask("Update Categorization app i18n: set custom CategoryOverview Model", RepositoryConstants.CONFIG, PATH_TO_CATEGORY_OVERVIEW_TEMPLATE, "modelClass", "com.kasinsky.magnolia.testapp.module.categorization.model.ExactCategoryOverviewModel");
        tasks.add(new NodeExistsDelegateTask("", PATH_TO_CATEGORY_OVERVIEW_TEMPLATE, setModelClassTask));
        tasks.add(new SetPropertyTask("Update MainAreaIntro app i18n: set custom i18nBasename", RepositoryConstants.CONFIG, PATH_TO_MAIN_AREAS_INTRO, "i18nBasename", "mgnl-i18n.info.magnolia.module.templatingkit.custom.messages"));
        tasks.add(new SetPropertyTask("Update Search Result component: set custom result model", RepositoryConstants.CONFIG, PATH_TO_SEARCH_RESULT_COMPONENT, "modelClass", SearchWithCategoryResultModel.class.getName()));
        tasks.add(new SetPropertyTask("Update access to dam-app", RepositoryConstants.CONFIG, PATH_DAM_APP_PERMISSIONS, "bbc-base", "bbc-base"));
        tasks.add(new SetPropertyTask("Update theme", RepositoryConstants.CONFIG, PATH_TO_THEME, "name", "bbc"));
        RemoveNodeTask removeSubscriderTask = new RemoveNodeTask("Remove default subscriber", "Remove default subscriber", "config", "/server/activation/subscribers/magnoliaPublic8080");
        RenameNodesTask renameNodesTask = new RenameNodesTask("Rename default subscriber", "Rename default subscriber", RepositoryConstants.CONFIG, "/server/activation/subscribers", "magnoliaPublic8080", "magnoliaPublic8089", NodeTypes.ContentNode.NAME);
        SetPropertyTask updateSubscribersTask = new SetPropertyTask("Update subscriber URL", RepositoryConstants.CONFIG, PATH_TO_DEFAULT_SUBSCRIBER, "URL", "http://localhost:8089");
        tasks.add(new IsAuthorInstanceDelegateTask("Update anonymous user", renameNodesTask));
        tasks.add(new IsAuthorInstanceDelegateTask("Update anonymous user", updateSubscribersTask, removeSubscriderTask));
        return tasks;
    }

}