/*******************************************************************************
 * Copyright (c) 2013. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.kasinsky.magnolia.testapp.module.categorization.model;

import com.google.common.base.Joiner;
import info.magnolia.cms.core.MgnlNodeType;
import info.magnolia.cms.util.QueryUtil;
import info.magnolia.module.templatingkit.functions.STKTemplatingFunctions;
import info.magnolia.module.templatingkit.search.SearchResultModel;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.templating.functions.TemplatingFunctions;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.text.MessageFormat;

public class SearchWithCategoryResultModel extends SearchResultModel {
    private static final Logger log = LoggerFactory.getLogger(SearchWithCategoryResultModel.class);
    public static final String WORKSPACE = "category";

    @Inject
    public SearchWithCategoryResultModel(Node content, TemplateDefinition definition, RenderingModel<?> parent,
                                         STKTemplatingFunctions stkFunctions, TemplatingFunctions templatingFunctions) {
        super(content, definition, parent, stkFunctions, templatingFunctions);
    }

    @Override
    protected String generateSimpleQuery(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        String queryString;
        if (input.contains(" ")) {
            queryString = "{1}";
        } else {
            queryString = "*{1}*";
        }
        StringBuilder searchQueryPattern = new StringBuilder("select * from nt:base where jcr:path like ''{0}/%'' and contains(*, ''" + queryString + "'') ");
        try {
            NodeIterator iterator = findCategories(input);
            if (iterator != null && iterator.hasNext()) {
                Node node = iterator.nextNode();
                searchQueryPattern.append("or (contains(categories, ''")
                        .append(node.getIdentifier())
                        .append("'')");
                while (iterator.hasNext()) {
                    node = iterator.nextNode();
                    searchQueryPattern.append(" and contains(categories, ''")
                            .append(node.getIdentifier())
                            .append("'')");

                }
                searchQueryPattern.append(")");
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        searchQueryPattern.append(" order by jcr:path");
        //escape single quote
        String searchString = input.replace("'", "''");
        return MessageFormat.format(searchQueryPattern.toString(), new String[]{this.getPath(), searchString});
    }

    public NodeIterator findCategories(String input) {

        if (StringUtils.isBlank(input)) {
            return null;
        }
        StringBuilder queryString = new StringBuilder("select * from mgnl:category where ");
        String[] tokens = input.split(" ");
        queryString.append("contains(*, '*")
                .append(Joiner.on("* OR *").join(tokens))
                .append("*')");
        try {
            return QueryUtil.search(WORKSPACE, queryString.toString(), javax.jcr.query.Query.SQL, MgnlNodeType.NT_CONTENT);
        } catch (RepositoryException e) {
            log.error(MessageFormat.format("{0} caught while parsing query for search term [{1}] - query is [{2}]", e.getClass().getName(), queryString, e.getMessage()), e);
        }

        return null;

    }


}
