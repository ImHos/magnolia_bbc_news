/*******************************************************************************
 * Copyright (c) 2013. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.kasinsky.magnolia.testapp.module.categorization.model;

import info.magnolia.cms.util.SelectorUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.wrapper.I18nNodeWrapper;
import info.magnolia.module.categorization.functions.CategorizationTemplatingFunctions;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.templating.functions.TemplatingFunctions;
import info.magnolia.util.EscapeUtil;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.jcr.Node;

public class ExactCategoryOverviewModel extends info.magnolia.module.categorization.model.CategoryOverviewModel {

    @Inject
    public ExactCategoryOverviewModel(Node content, TemplateDefinition definition, RenderingModel parent,
                                      CategorizationTemplatingFunctions catTemplatingFunction,
                                      TemplatingFunctions templatingFunction) {
        super(content, definition, parent, catTemplatingFunction, templatingFunction);
    }

    @Override
    public String getDisplayName() {
        try {
            String categoryName = EscapeUtil.escapeXss(SelectorUtil.getSelector(0));
            Node categoryNodeByName = catTemplatingFunction.getCategoryNodeByName(categoryName);
            I18nNodeWrapper nodeWrapper = new I18nNodeWrapper(categoryNodeByName);
            String displayName = PropertyUtil.getString(nodeWrapper, "displayName");
            if (StringUtils.isEmpty(displayName)) {
                return categoryName;
            }
            return displayName;
        } catch (Exception e) {
            return null;
        }
    }

}
