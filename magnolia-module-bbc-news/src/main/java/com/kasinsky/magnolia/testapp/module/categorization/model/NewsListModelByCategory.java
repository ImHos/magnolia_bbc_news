/*******************************************************************************
 * Copyright (c) 2013. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.kasinsky.magnolia.testapp.module.categorization.model;

import info.magnolia.cms.util.SelectorUtil;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.categorization.functions.CategorizationTemplatingFunctions;
import info.magnolia.module.templatingkit.functions.STKTemplatingFunctions;
import info.magnolia.module.templatingkit.templates.components.NewsListModel;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.templating.functions.TemplatingFunctions;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class NewsListModelByCategory extends NewsListModel {
    protected final CategorizationTemplatingFunctions catTemplatingFunction;

    @Inject
    public NewsListModelByCategory(Node content, TemplateDefinition definition, RenderingModel parent,
                                   STKTemplatingFunctions stkFunctions, TemplatingFunctions templatingFunctions, CategorizationTemplatingFunctions catTemplatingFunction) {
        super(content, definition, parent, stkFunctions, templatingFunctions);
        this.catTemplatingFunction = catTemplatingFunction;
    }

    @Override
    protected List<Node> search() throws RepositoryException {
        String name = getCategory();
        Node categoryNode = catTemplatingFunction.getCategoryNodeByName(name);
        String andClause = null;
        if (categoryNode != null) {
            andClause = "CONTAINS(categories, '" + categoryNode.getIdentifier() + "')";
        }
        return stkFunctions.getContentListByTemplateCategorySubCategory(getSearchRoot(), getCategoryName(), getSubcategoryName(), Integer.MAX_VALUE, andClause, null);
    }

    private String getCategory() {
        try {
            PropertyIterator categoryIterator = MgnlContext.getAggregationState().getMainContentNode().getProperties("category");
            if (categoryIterator.hasNext()) {
                String categoryPath = categoryIterator.nextProperty().getString();
                if (!isBlank(categoryPath)) {
                    return categoryPath.substring(categoryPath.lastIndexOf("/") + 1);
                }
            }
            return SelectorUtil.getSelector(0);
        } catch (RepositoryException e) {
            return SelectorUtil.getSelector(0);
        }
    }


}
