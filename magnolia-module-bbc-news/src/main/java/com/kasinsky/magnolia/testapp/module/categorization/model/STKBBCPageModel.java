/*******************************************************************************
 * Copyright (c) 2013. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.kasinsky.magnolia.testapp.module.categorization.model;

import com.google.common.base.Function;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.wrapper.I18nNodeWrapper;
import info.magnolia.module.templatingkit.functions.STKTemplatingFunctions;
import info.magnolia.module.templatingkit.templates.pages.STKPage;
import info.magnolia.module.templatingkit.templates.pages.STKPageModel;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.templating.functions.TemplatingFunctions;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.jcr.Node;
import java.util.List;

import static com.google.common.collect.Collections2.transform;
import static com.google.common.collect.Lists.newArrayList;

public class STKBBCPageModel extends STKPageModel {
    @Inject
    public STKBBCPageModel(Node content, STKPage definition, RenderingModel parent, STKTemplatingFunctions stkFunctions,
                           TemplatingFunctions templatingFunctions, DamTemplatingFunctions damTemplatingFunctions) {
        super(content, definition, parent, stkFunctions, templatingFunctions, damTemplatingFunctions);
    }

    @Override
    public List<?> getCategories() {
        List<ContentMap> categories = super.getCategories();
        return newArrayList(transform(categories, new Function<ContentMap, ContentMap>() {

            public ContentMap apply(ContentMap input) {
                I18nNodeWrapper nodeWrapper = new I18nNodeWrapper(input.getJCRNode());
                return new ContentMap(nodeWrapper);
            }
        }));
    }
}
